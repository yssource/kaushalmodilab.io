#!/usr/bin/env bash

set -euo pipefail # http://redsymbol.net/articles/unofficial-bash-strict-mode
IFS=$'\n\t'

# First cd to the root of the git repo.
cd "$(git rev-parse --show-toplevel)" || exit
if [[ -d ./public ]];
then
    rm -rf ./public
fi
hugo --ignoreCache

rm ./public/google4a938eaf9bbacbcd.html
# htmltest -l0
htmltest
