#!/usr/bin/env bash

# Wrapper script for Netlify/CI hugo builds

set -euo pipefail # http://redsymbol.net/articles/unofficial-bash-strict-mode
IFS=$'\n\t'

# Initialize variables
debug=0

extra_args=''

while [ $# -gt 0 ]
do
    case "$1" in
        "-D"|"--debug" ) debug=1;;
        * ) extra_args="${extra_args} $1";;
    esac
    shift # expose next argument
done

main () {
    if [[ ${debug} -eq 1 ]]
    then
        echo "Debug mode"
    fi

    if [[ ! -z ${DEPLOY_PRIME_URL+x} ]]
    then
        if [[ "${DEPLOY_PRIME_URL}" != https://master--* ]] # Ignore DEPLOY_PRIME_URL for master branch commits
        then
            echo "DEPLOY_PRIME_URL = ${DEPLOY_PRIME_URL}"
            # Branch and Deploy contexts
            #
            # If your Netlify site name is "foo" (i.e. you have
            # https://app.netlify.com/sites/foo), and your git repo
            # branch is "bar", Netlify auto-deploys your branch at
            # "https://bar--foo.netlify.com" (regardless of your
            # setting a custom domain for your site). The
            # DEPLOY_PRIME_URL env var would be auto-set to
            # "https://bar--foo.netlify.com" in this case.
            HUGO_BASEURL="${DEPLOY_PRIME_URL}/" # Add the trailing forward slash to Hugo BaseURL
            echo "HUGO_BASEURL = ${HUGO_BASEURL}"
        fi
    fi
    echo ""

    echo "[1/2] Generating site .."
    # https://gohugo.io/
    tar xf ./bin/hugo_DEV-Linux-64bit.tar.xz
    ./hugo version
    hugo_cmd="./hugo ${extra_args}"
    echo "${hugo_cmd}"
    eval "${hugo_cmd}"

    echo '[2/2] Adding rel="noopener" for target="_blank" links ..'
    # Open new tabs using `rel="noopener"` to improve performance and prevent
    # security vulnerabilities.
    # https://developers.google.com/web/tools/lighthouse/audits/noopener
    # - Find all .html files in public/.
    # - In those files, then filter down to only the lines that have an <a..>
    #   tag with *only* href and target properties.  This is to prevent false
    #   search/replace.
    # - Then replace «target="_blank"» with «target="_blank" rel="noopener"»
    #   only in those lines.
    find ./public -name "*.html" -exec sed -r -i '/<a href=[^ ]+ target="?_blank"?>/ s/target=("?)_blank\1/\0 rel=\1noopener\1/g' {} +
    echo ""

    echo "Done!"
}

main
