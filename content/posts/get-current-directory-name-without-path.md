+++
title = "Get current directory name without path"
date = "2014-03-04T13:48:01-05:00"
categories = ["awk", "shell"]
+++

This post shows how to get the current directory name without the
preceeding path, using `awk` or `rev`+`cut` or the boring `basename`.

<!--more-->

- awk
```sh
pwd | awk -F/ '{print $NF}'
```

- rev and cut
```sh
pwd | rev | cut -d/ -f 1 | rev
```

- basename
```sh
basename `pwd`
```
