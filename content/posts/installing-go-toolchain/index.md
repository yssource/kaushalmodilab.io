+++
title = "Installing go toolchain"
author = ["Kaushal Modi"]
description = """
  "Installing" `go` is simply extracting its release archive, putting it
  somewhere in you `$HOME` and pointing `GOROOT` and `PATH` env vars to
  it.
  """
date = 2017-02-24T01:33:47-05:00
tags = ["toolchain", "golang"]
categories = ["unix"]
draft = false
creator = "Emacs 27.0.50 (Org mode 9.1.14 + ox-hugo)"
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [Installing `go`](#installing-go)
- [Updating `go`](#updating-go)

</div>
<!--endtoc-->

<span class="timestamp-wrapper"><span class="timestamp">&lt;2018-05-17 Thu&gt;</span></span>
: Add "Updating go" section.

There are **two** reasons why I suggest installing `go` to anyone,
whether they are Go developers, or not (like me).

1.  You can then build amazing utilities like [peco](https://github.com/peco/peco), [hugo](https://github.com/gohugoio/hugo) and [noti](https://github.com/variadico/noti).
2.  **It's easy!**


## Installing `go` {#installing-go}

Below instructions are for installing `go` on a 64-bit GNU/Linux
machine, and using `tcsh` shell. But similar steps should work for any
other OS and shell.

1.  Download the _tar.gz_ for the latest _linux-amd64_ binaries from
    <https://golang.org/dl/>.
2.  Extract it to some place in your `$HOME`. I extract it to
    `${HOME}/go/`[^fn:1].
3.  Create a directory where you would want to install the `go`
    packages.

    ```shell
    mkdir -p ~/go.apps
    ```
4.  Set the following environment variables[^fn:2], and also save them to
    your shell config:

    ```tcsh
    setenv GOROOT ${HOME}/go # go root
    setenv GOPATH ${HOME}/go.apps # for go applications
    ```
5.  Add the `${GOROOT}/bin` and `${GOPATH}/bin` directories to your
    `$PATH`.

Now you can install any `go` application!

For instance, `noti` is a nice little utility that triggers an alert
(desktop popup, _Pushbullet_ notification, etc.) when a process
finishes.  From its [installation notes](https://github.com/variadico/noti#installation), you just run the below to
install it:

```text
go get -u github.com/variadico/noti/cmd/noti
```

Apart from the `go` applications I suggested here, _go_ out and explore
more -- `go get` them :grin:


## Updating `go` {#updating-go}

1.  Delete the existing `$GOROOT` directory (**not `GOPATH`!**)

    ```shell
    rm -rf ~/go   # as that is my GOROOT
    ```
2.  Download the _tar.gz_ for the latest _linux-amd64_ binaries.
3.  Extract it to the same `$GOROOT` (`~/go` in my case).

[^fn:1]: I prefer to not add the version number to my `go` installation folder. That way, when I want to update it, I simply `rm -rf` it and put in the new version.. and I don't need to update `GOROOT` or `PATH`.
[^fn:2]: You can refer to these official `go` references [[1](https://golang.org/doc/install#tarball),[2](https://golang.org/doc/install#testing)] for further information on these variables.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
