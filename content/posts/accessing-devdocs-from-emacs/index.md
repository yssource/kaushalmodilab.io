+++
title = "Accessing Devdocs from Emacs"
author = ["Kaushal Modi"]
description = """
  Spoiled by being able to access in-built docs in Emacs at fingertips,
  here's an attempt to _kind-of_ do that for Nim documentation too,
  using `devdocs.io`.
  """
date = 2018-05-10T12:45:00-04:00
images = ["nim-devdocs-screenshot.png"]
tags = ["devdocs", "documentation", "nim", "search", "package"]
categories = ["emacs"]
draft = false
creator = "Emacs 27.0.50 (Org mode 9.1.14 + ox-hugo)"
[syndication]
  twitter = 994623657195593728
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [JSON Docs](#json-docs)
- [Devdocs.io](#devdocs-dot-io)
    - [Christopher Wellon's `devdocs-lookup`](#christopher-wellon-s-devdocs-lookup)
    - [Making `devdocs-lookup` DWIM](#making-devdocs-lookup-dwim)
- [Using `devdocs-lookup`](#using-devdocs-lookup)
- [Demo](#devdocs-lookup-demo)
- [Code](#devdocs-lookup-code)

</div>
<!--endtoc-->

Nim lang has good documentation for all its `stdlib` functions
[online](https://nim-lang.org/docs/lib.html). But the Emacs user in me does not like to switch back and
forth between the Nim code in Emacs buffers and the docs outside in an
external browser.

Well.. a solution to that, that this post is about, _still_ needs one
to look up the Nim docs in an external browser.. but the workflow is a
bit better --- You don't need to manually launch the doc site, and you
don't need to then manually type in the search query.

<div class="note">
  <div></div>

If you want to skip the history and code analysis, you can directly
jump to the [Demo](#devdocs-lookup-demo) or the [Final Code](#devdocs-lookup-code).

</div>


## JSON Docs {#json-docs}

I tried asking folks on [r/nim](https://www.reddit.com/r/nim/comments/8ia1xk/is%5Fthere%5Fa%5Fway%5Fto%5Faccess%5Fthe%5Fnim%5Fdocs%5Ffrom%5Fthe/) if there was a good solution for
_in-editor_ Nim doc access. [**/u/PMunch**](https://www.reddit.com/user/PMunch) from Reddit gave a wonderful
[solution](https://www.reddit.com/r/nim/comments/8ia1xk/is%5Fthere%5Fa%5Fway%5Fto%5Faccess%5Fthe%5Fnim%5Fdocs%5Ffrom%5Fthe/dyqcb2m/)---To generate JSON docs for the Nim stdlib, and then parse
those to display the docs within Emacs.

&nbsp;&nbsp;&nbsp;&nbsp;I would love that solution!<br />

.. just that I don't know how to get a nice single `.json` for the
whole of Nim documentation.

_If someone knows how to do that, please let me know._


## Devdocs.io {#devdocs-dot-io}

So I continued my search online.. I was looking if someone had already
implemented a way to access Nim docs from the command line, and that
somehow led me to <https://devdocs.io/nim/>!

And after searching further for _"devdocs Emacs"_, I found these two
Emacs packages:

-   <https://github.com/xuchunyang/DevDocs.el>
-   <https://github.com/skeeto/devdocs-lookup>


### Christopher Wellon's `devdocs-lookup` {#christopher-wellon-s-devdocs-lookup}

After reviewing the two packages, I decided to build my solution
further upon the `devdocs-lookup` package[^fn:1] by _Christopher
Wellons_ aka [**@skeeto**](https://www.github.com/skeeto) from GitHub.

Here's why ---

1.  He first fetches the whole JSON _search index_ from docs.devdocs.io
    for the picked "subject" (which would be "Nim" for this post). So
    the search index for Nim documentation would be at
    <https://docs.devdocs.io/nim/index.json>[^fn:2].

    The retrieved JSON is then parsed using `json-read`.

    <a id="code-snippet--devdocs-index"></a>
    {{< highlight emacs-lisp "hl_lines=5 13" >}}
    (defun devdocs-index (subject &optional callback)
      "Return the devdocs.io index for SUBJECT, optionally async via CALLBACK."
      (cl-declare (special url-http-end-of-headers))
      (let ((index (gethash subject devdocs-index))
            (url (format "%s/%s/index.json" devdocs-base-index-url subject)))
        (cond ((and index callback)
               (funcall callback index))
              ((and index (not callback))
               index)
              ((and (not index) (not callback))
               (with-current-buffer (url-retrieve-synchronously url nil t)
                 (goto-char url-http-end-of-headers)
                 (setf (gethash subject devdocs-index) (json-read))))
              ((and (not index) callback)
               (url-retrieve
                url
                (lambda (_)
                  (goto-char url-http-end-of-headers)
                  (setf (gethash subject devdocs-index) (json-read))
                  (funcall callback (gethash subject devdocs-index))))))))
    {{< /highlight >}}


    <div class="src-block-caption">
      <span class="src-block-number"><a href="#code-snippet--devdocs-index">Code Snippet 1</a></span>:
      Function to fetch the search index from devdocs.io and parse the JSON
    </div>
2.  The `name` and `path` properties from the parsed JSON are then
    stored in an association list here:

    <a id="code-snippet--devdocs-entries"></a>
    {{< highlight emacs-lisp "hl_lines=4 5" >}}
    (defun devdocs-entries (subject)
      "Return an association list of the entries in SUBJECT."
      (cl-loop for entry across (cdr (assoc 'entries (devdocs-index subject)))
               collect (cons (cdr (assoc 'name entry))
                             (cdr (assoc 'path entry)))))
    {{< /highlight >}}


    <div class="src-block-caption">
      <span class="src-block-number"><a href="#code-snippet--devdocs-entries">Code Snippet 2</a></span>:
      Function to store the <code>name</code> and <code>path</code> properties to alists
    </div>

    So a JSON entry like this:

    ```json
    {
        "name": "os.walkDirRec",
        "path": "os#walkDirRec.i,string",
        "type": "os"
    }
    ```

    would translate to this Emacs-Lisp alist element:

    ```emacs-lisp
    ("os.walkDirRec" . "os#walkDirRec.i,string")
    ```
3.  Then the _list_ of all the _car_'s of such elements is used to
    create a collection of entries for completion:

    <a id="code-snippet--devdocs-read-entry"></a>
    {{< highlight emacs-lisp "hl_lines=3" >}}
    (defun devdocs-read-entry (subject)
      "Interactively ask the user for an entry in SUBJECT."
      (let ((names (mapcar #'car (devdocs-entries subject)))
            (hist (intern (format "devdocs--hist-%s" subject))))
        (unless (boundp hist)
          (set hist nil))
        (completing-read "Entry: " names nil :match nil hist)))
    {{< /highlight >}}


    <div class="src-block-caption">
      <span class="src-block-number"><a href="#code-snippet--devdocs-read-entry">Code Snippet 3</a></span>:
      Function to show completion list based on "names" from the JSON-parsed database
    </div>
4.  And finally, for the selected _name_, the associated _path_ is
    retrieved from that _alist_, and we browse to that path using the
    Emacs `browse-url` function. _User can of course configure the
    browser to be used when that function is called._

    <a id="code-snippet--devdocs-lookup"></a>
    {{< highlight emacs-lisp "hl_lines=7 9" >}}
    (defun devdocs-lookup (subject entry)
      "Visit the documentation for ENTRY from SUBJECT in a browser."
      (interactive
       (let* ((subject (devdocs-read-subject))
              (entry (devdocs-read-entry subject)))
         (list subject entry)))
      (let ((path (cdr (assoc entry (devdocs-entries subject)))))
        (when path
          (browse-url (format "%s/%s/%s" devdocs-base-url subject path))
          :found)))
    {{< /highlight >}}


    <div class="src-block-caption">
      <span class="src-block-number"><a href="#code-snippet--devdocs-lookup">Code Snippet 4</a></span>:
      Function to browse the doc page associated with the user-selected "name"
    </div>

All of that worked beautifully. As I used it a few times though, I
felt a need to add a touch of <abbr title="Do What I Mean">DWIM</abbr> to that.


### Making `devdocs-lookup` DWIM {#making-devdocs-lookup-dwim}

Here are the 2 things that I wanted to happen automatically:


#### Auto-select subject based on `major-mode` if possible {#auto-select-subject-based-on-major-mode-if-possible}

In the original code, if I used `devdocs-lookup` function, I needed to
manually select the "Nim" subject even when I called that function
from a `nim-mode` buffer. _At least for my use cases, I would want to
access only Nim docs if I am looking up devdocs while in a Nim code
buffer._

The package has an interesting function called `devdocs-setup` which
would generate a function specific to each subject.. so for "Nim"
subject, it would generate a `devdocs-lookup-nim` function.

But I wanted to avoid calling `devdocs-setup` too.

So below is what I did:

<a id="code-snippet--devdocs-lookup-modified"></a>
{{< highlight emacs-lisp "hl_lines=4-12" >}}
(defun devdocs-lookup (subject entry)
  "Visit the documentation for ENTRY from SUBJECT in a browser."
  (interactive
   (cl-letf (((symbol-function 'string-match-case-insensitive)
              (lambda (str1 str2)
                (string= (downcase str1) (downcase str2)))))
     (let* ((major-mode-str (replace-regexp-in-string "-mode" "" (symbol-name major-mode)))
            ;; If major mode is `nim-mode', the ("Nim" "nim") element
            ;; will be auto-picked from `devdocs-subjects'.
            (subject-dwim (cadr (assoc major-mode-str devdocs-subjects
                                       #'string-match-case-insensitive)))
            (subject (or subject-dwim (devdocs-read-subject)))
            (entry (devdocs-read-entry subject)))
       (list subject entry))))
  (let ((path (cdr (assoc entry (devdocs-entries subject)))))
    (when path
      (browse-url (format "%s/%s/%s" devdocs-base-url subject path))
      :found)))
{{< /highlight >}}


<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--devdocs-lookup-modified">Code Snippet 5</a></span>:
  Modification of the original <code>devdocs-lookup</code> &#x2013; Now auto-selects the subject if the <code>major-mode</code> matches.
</div>


#### Auto-filter using the symbol at point {#auto-filter-using-the-symbol-at-point}

The second thing that I wanted to work upon was to make Emacs kind of
"know" what I was trying to search.

Originally, `devdocs-read-entry` would always show the completion-list
pointing at the first entry. I wanted to make that a bit more
intelligent.. If my point were on the `walkDirRec` _proc_ identifier
on a line like below,

```nim
for filepath in walkDirRec(appPath, yieldFilter={pcFile}):
```

I wanted the collection to narrow down to only the entries that
matched "walkDirRec".

Below is my modification to do that.

<a id="code-snippet--devdocs-read-entry-modified"></a>
{{< highlight emacs-lisp "hl_lines=5 8" >}}
(defun devdocs-read-entry (subject)
  "Interactively ask the user for an entry in SUBJECT."
  (let ((names (mapcar #'car (devdocs-entries subject)))
        (hist (intern (format "devdocs--hist-%s" subject)))
        (init (symbol-name (symbol-at-point))))
    (unless (boundp hist)
      (set hist nil))
    (completing-read (format "Entry (%s): " subject) names nil :require-match init hist)))
{{< /highlight >}}


<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--devdocs-read-entry-modified">Code Snippet 6</a></span>:
  Modification of the original <code>devdocs-read-entry</code> &#x2013; Now auto-filters the entries that match the symbol at point.
</div>

Above function just pre-sets the filter.. If the user wants to change
the search string, they can still do that.


## Using `devdocs-lookup` {#using-devdocs-lookup}

Finally, I like the [`key-chord.el`](https://www.emacswiki.org/emacs/key-chord.el) package. So using that, I bind the
`??` key-chord to the modified `devdocs-lookup` function.

So if I want to look up the docs for `walkDirRec` on that line in the
above example, I just move the point there, and hit `??`, and the docs
for that will pop up in my browser..

-   No manual launching of the browser.
-   No manual typing of the search string.


## Demo {#devdocs-lookup-demo}

It won't be fun if I did not end this post without a demo. So here it
is ---

<a id="org1f370f4"></a>

{{< figure src="nim-devdocs-screenshot.png" caption="Figure 1: Click the above image to see the devdocs.io access from Emacs in action (GIF)" link="nim-devdocs.gif" >}}


## Code {#devdocs-lookup-code}

You can find the modified `devdocs-lookup` code [here](https://github.com/kaushalmodi/devdocs-lookup).

[^fn:1]: All of the code snippets from Christopher Wellon's `devdocs-lookup` that now follow in this post are from [this commit](https://github.com/skeeto/devdocs-lookup/tree/021c3c95030b4ee0d83a6961804c7a347faa72de).
[^fn:2]: If you visit that file in Firefox, it will show up in a wonderful formatted form with collapsible drawers, search, etc.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
